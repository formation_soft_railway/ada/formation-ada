-- Ajout des librairies
with Ada.Text_IO;

procedure Main is

   -- pragma Optimize(Off);

   -- Declaration de variables
   a : integer := 3;
   -- b : integer := integer'first; -- plus petit entier
   -- c : integer := integer'last; -- plus grand entier
   --type tb is range 0 .. 1_000_000
   type tb is mod 1_000_001; -- valeurs de 0 � 1000000, revient � 0 si d�passement
   b : tb;

   c : float;
   type td is digits 3;
   d : td := 123.45;

begin
   Ada.text_IO.Put_line("Bienvenue dans l'agence de voyage");
   a := a + 5;
   b:= 999_999;
   b := b+2;
   Ada.Text_IO.Put_line ("b = " & b'Image);
   Ada.Text_IO.Put_line ("d = " & d'Image);
   --Ada.Text_IO.Put_line (c'Image);
   Ada.Text_IO.Put("a = " & a'Image);
end Main;
