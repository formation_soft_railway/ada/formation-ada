package Amerique is

   type tprix is digits 6 range 0.0 .. 10000.0;
   procedure faire_Canada (avion : tprix := 1000.0; hotel : tprix := 100.0);

-- Tous les �l�ments d�clar�s ci-dessous ne sont accessibles que dans ce package et
-- les packages enfants. (non visibles depuis les zones o� on fait un include)
private
   -- Pour contraindre les valeurs rentrees
   -- A definir avant car necessaire � l'appel de faire_Canada
   type tduree is range 0 .. 100;

end Amerique;
