with Ada.Text_IO;
use Ada.Text_IO; -- N�cessaire d'ajouter le "with" avant. Plus n�cessaire de mettre
                 -- Ada.Text_IO.xxxx
with Ada.Integer_Text_IO;

package body Amerique is



   -- ##########################################################################
   --                           get_duree
   -- ##########################################################################

   function get_duree return tduree is
      tmp_entree : integer;
      duree : tduree;
   begin
      -- Saisie clavier d'un entier
      Ada.text_IO.Put_line("Entrez la duree de votre voyage (jours) : ");
      Ada.Integer_text_IO.Get(tmp_entree);
      duree := tduree(tmp_entree);
      return duree;
   end get_duree;

   -- ##########################################################################
   --                           Faire_Canada
   -- ##########################################################################

   -- Declaration de procedure avec valeur par d�faut
   procedure faire_Canada (avion : tprix := 1000.0; hotel : tprix := 100.0) is

      -- ********************* Declaration variables ****************************

      type tpromo is digits 3 range 0.0 .. 100.0;
      promo : tpromo := 20.0;

      -- avion : tprix := 860.0;
      duree : tduree := 7;
      -- hotel : tprix := 48.0;
      total : tprix;

      -- Type enumeres
      -- Il est possible de faire des operations entre des donnees tjours et tweekend
      type tjours is (lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche);
      type ttweekend is (samedi, dimanche);
      depart : tjours := lundi;

      -- type deriv�
      type tjsemaine is new tjours range lundi .. vendredi;

      -- sous types
      -- Compatibles avec le type "m�re"
      subtype tgrandweekend is tjours range vendredi .. dimanche;
      arrivee : tgrandweekend := dimanche;
      voldenuit : boolean := depart /= arrivee;

      -- Alias
      subtype tgdweekend is tgrandweekend;

      -- Type caract�re
      -- c1 : character := 'A';

      -- Type chaine de caract�res
      s1 : string := "Ok !";
      infos : string := "Voyages au Canada : -" & promo'Image &
        "% sur " & avion'Image & "e et " & duree'Image &
        "j a " & hotel'Image & "e/j.";

      -- Initialisation d'une chaine sans l'initialiser
      -- Il faut mettre exactement la bonne taille sinon erreur lev�e
      infostotal : string (1..28);

      -- Creation de tableau
      type ttabindex is range 0..2;
      type ttab1 is array(ttabindex) of character;
      type ttab2 is array (0..2) of character;
      tab1 : ttab1 := ('A', 'B', 'C');

      type ttabDistance is array(tduree) of integer;
      tabKm : ttabDistance;

      -- type ttab3 is array (ttabindex range <>) of integer;
      --tkm : ttab3 (ttabindex);

      type t2dims is array (1..4, 2..3) of float;

      -- *************************** Begin ************************************

   begin
      Ada.text_IO.Put_line("Bienvenue dans l'agence de voyage");

      duree := get_duree;

      -- Affichage de "Voyages au Canada : -20% sur 860e et 7j � 48e/j."
      Ada.text_IO.Put_line(infos);

      -- ------------------------- Prix du voyage ----------------------------

      -- Total du voyage
      total := tprix((100.0-float(promo))/100.0*(float(avion)+float(duree)*float(hotel)));
      infostotal := "Total Canada : " & total'Image & "e";
      Ada.text_IO.Put_line(infostotal);

      Ada.text_IO.Put_line("Depart le : " & depart'Image & ".");

      -- Comparaison
      Ada.text_IO.Put_line("Depart dimanche ? " & boolean(depart=dimanche)'Image & ".");

      --  ------------------------ Compagnie a�rienne ------------------------

      -- En general : Air Canada
      -- Voyage court (<7) : Quebec'Air
      -- Voyage de 7, 14, 21, 28j : Can'jet
      if duree < 7 then
         Ada.text_IO.Put_line("Compagnie aerienne : Quebec'Air");
      elsif duree = 7 or duree = 14 or duree = 21 or duree = 28 then
         Ada.text_IO.Put_line("Compagnie aerienne : Can'jet");
      else
         Ada.text_IO.Put_line("Compagnie aerienne : Air Canada");
      end if;

      --  ------------------------  Programme du voyage ------------------------

      -- Jour 1 : avion
      -- Jour 2 : marche
      -- Jour 3 : marche
      -- Jour 4 : marche
      -- Jour 5 : repos
      -- ...
      -- Jour xx : avion

      -- En + : stocker dans un tableau le nombre de km parcourus
      -- Avion : 6500 km
      -- Marche : 22 km
      for numJours in 1 .. duree loop -- inutile de declarer numJours => declaration implicite dans le loop
         if numJours = 1 or numJours = duree then
            Ada.text_IO.Put_line("Jour " & numJours'Image & " : avion");
            tabKm(numJours) := 6500;
         elsif numJours mod 4 /= 1 then
            Ada.text_IO.Put_line("Jour " & numJours'Image & " : marche");
            tabKm(numJours) := 22;
         else
            Ada.text_IO.Put_line("Jour " & numJours'Image & " : repos");
            tabKm(numJours) := 0;
         end if;
      end loop;

      -- ------------------------  Distance parcourue ------------------------
      -- Bloc "anonyme"
      declare
         nbKm : integer := 0;
      begin
         -- Refaire une boucle pour calculer le nombre de km total parcourus
         for numJours in 1 .. duree loop
            nbKm := nbKm + tabKm(numJours);
         end loop;
         Ada.text_IO.Put_line("Total km : " & nbKm'Image & "km.");

         -- Modification valeur tableau
         tab1(0):='K';
         tab1(tab1'first) := 'J';
      end;

   end faire_Canada;

end Amerique;
