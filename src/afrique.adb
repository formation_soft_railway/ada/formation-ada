with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
with Ada.Exceptions;

package body Afrique is

   -- ------ Fonction Get_Prix_Namibie ------ --
   function Get_Prix_Namibie (j:positive) return positive

   -- Activation des verifications :
   -- edit > project properties > switches > Ada > Enable assertions
     -- Verification de preconditions avec compilateur Spark
     with pre => j>1 and j<50
   is
   begin
      return j*37+1210;
   end Get_Prix_Namibie;

   -- ------ Procedure faire_Namibie ------ --
   procedure faire_Namibie is

      -- ********************* Declaration variables ****************************

      j : positive;
      ex_que7 : Exception;

      -- ******************************** Begin *********************************

   begin
      Put_line("Namibie");
      Put_line("Combien de jours partez-vous ?");
      Get(j);

      -- Declencher des exceptions
      if j mod 7 /= 0 then
         Ada.Exceptions.Raise_Exception(ex_que7'Identity, "Avion que le samedi");
         -- ou (autre version)
         -- raise ex_que7 with "Avion que le samedi";
      end if;

      Put_line("Vous partez " & j'Image & " jours");
      Put_line("Prix TTC : " & Get_Prix_Namibie(j)'Image & "e");

   -- Traitement d'exception
   exception
      -- Le traitement est lie a l'objet ex_que7 (pas a un type d'exception)
      when e:ex_que7
         => Put_line("Exception levee : " & Ada.Exceptions.Exception_Message(e));

      -- Erreur si non positif
      -- when e:constraint_error

      -- Erreur generique
      when e:others
         => Put_line("Exception others : " & Ada.Exceptions.Exception_Message(e));

   end faire_Namibie;

      -- ************************************************************************

end Afrique;
