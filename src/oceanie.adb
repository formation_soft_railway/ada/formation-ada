with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Numerics.Discrete_Random;
with Ada.Containers.Vectors;

package body Oceanie is

   procedure faire_Australie is

      type tloterienum is range 1..20;
      -- Type g�n�rique
      package rloterie is new ada.numerics.discrete_random(tloterienum);
      genloterie : rloterie.generator;

      package vLoterie is new Ada.Containers.Vectors (Index_Type => Positive, Element_Type => tloterienum);
      vTirage : vLoterie.Vector;
      tmp : tloterienum;


      task tirage;
      task affichage is
         entry finTirage; -- finTirage est le declencheur de la tache
      end affichage;

      -- Tirage au sort
      task body tirage is
      begin
         Put_line ("Debut tirage");

         -- Tirer 4 numeros (boucle)
         -- a chaque fois, si le numero a deja ete tire, en tirer un nouveau (avec Pause)
         while integer(vTirage.length)<4 loop
            delay 1.0; -- Attente de 1s
            tmp := rloterie.random(genloterie);
            Put_Line ("...");
            if not vTirage.contains(tmp) then
               vTirage.append(tmp);
               -- Put_Line("Tirage : " & tmp'Image);
--              else
--                 Put_Line ("...");
            end if;
         end loop;
         affichage.finTirage; -- Donne le declencheur pour la tache affichage
         Put_Line ("Fin tirage");

      end tirage;

      -- Affichage du tirage au sort
      task body affichage is
      begin
         accept finTirage; -- attend finTirage pour se lancer
         Put_Line("Resultats de la loterie : ");
         for i in 1..4 loop
            Put_Line("Tirage " & i'Image & " : " &  tloterienum'Image(vTirage(i)));
         end loop;
         Put_Line("Fin Australie");
      end affichage;

   begin
      Put_Line("Australie");
      rloterie.reset(genloterie);

   end faire_Australie;

end Oceanie;
