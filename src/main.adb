pragma Profile(Ravenscar);  -- restrictions pour les systemes temps reels critiques

-- Ajout des librairies
with Amerique;
use Amerique;
with Europe;
use Europe;
with Afrique;
use Afrique;
with Asie;
use Asie;
with Oceanie;
use Oceanie;
with Ada.Text_IO;
use Ada.Text_IO;

procedure Main is

begin
   -- Possibilite d'appeler avec ou sans arguments car valeurs par d�faut definies
   -- faire_Canada (860.0,48.0);
   --     faire_Canada(860.0);
   --     faire_Canada;
   --     faire_Canada(hotel => 50.0);

   --     faire_Espagne;
   --     faire_Italie;
   -- faire_Namibie;
   -- faire_Russie;
   -- faire_Japon;
   faire_Australie;
   Put_line("Au revoir");

end Main;
