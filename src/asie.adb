with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
with Ada.Strings;
use Ada.Strings;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Ada.Text_IO.Unbounded_IO;
with Ada.Calendar;
use Ada.Calendar;
with Ada.Calendar.Formatting;
with Ada.Containers.Vectors;

package body Asie is

   procedure faire_Russie is
      tel : string (1..10);
      nom, dest : Unbounded_String;
      tel_l : natural;
      dest1, dest2 : natural;
      maintenant : Ada.Calendar.Time := Ada.Calendar.Clock;
      depart : Ada.Calendar.Time;
      preparation: duration := 10.0; -- duree en seconds
      fichier : Ada.Text_IO.File_Type;
   begin
      Put_Line("Russie");

      Put_Line("Destination ?");
      -- Get_line(dest, dest_l);
      Ada.Text_IO.Unbounded_IO.Get_line(dest);
      -- Fonction declaree dans le package Ada.Strings.Unbounded
      -- Both declare dans Ada.Strings
      -- Permet de supprimer les espaces
      --dest:= Trim(dest, Both);
      dest1:= Index_Non_Blank(dest,1, Forward);
      dest2:= Index_Non_Blank(dest,length(dest), Backward);
      Unbounded_Slice(dest, dest, dest1, dest2);

      Put_line("Nom ?");
      Ada.Text_IO.Unbounded_IO.Get_line(nom);
      nom := Trim(nom, Both);

      Put_line("Telephone ?");
      Get_line(tel, tel_l);

      -- Contraint l'affichage a la taille effective de la chaine de caracteres
      -- Les caracteres apres la taille recuperee sont inconnus
      --Put_line("Voyage en " & dest(1..dest_l) & " enregistre pour " & To_String(nom) & ".");
      Put_line("Voyage en " & To_String(dest) & " enregistre pour " & To_String(nom) & ".");

      Put_line("Annee en cours : " & Ada.Calendar.Year(maintenant)'Image);
      -- Formattage de la date
      Put_line("Maintenant : " & Ada.Calendar.Formatting.Image(maintenant));

      -- maintenant + d => time
      -- maintenant - d => time
      -- d+d => duration
      -- d-d => duration
      -- maintenant - maintenant => duration
      -- maintenant + maintenant => impossible

      -- Afficher : depart au mieux le ../.. (4 sem. et 3 jr + tard)
      preparation := duration((4*7+3)*24*3600);
      depart := maintenant + preparation;
      Put_line("depart au mieux le " & Ada.Calendar.Day(depart)'Image & "/"
               & Ada.Calendar.Month(depart)'Image);

      -- fabrique un fichier russie.csv, chaque ligne ressemble �
      -- Moscou,Bob,0654983098
      -- Gestion d'exception si le fichier n'existe pas lorsqu'on essaye de l'ouvrir
      begin
         Ada.Text_IO.Open(fichier, Ada.Text_IO.Append_File, "russie.csv");
         exception
            when e:Ada.Text_IO.name_error => Ada.Text_IO.Create(fichier, Ada.Text_IO.Append_File, "russie.csv");
      end;
      Ada.Text_IO.Put_Line(fichier, To_String(dest) & "," & To_String(nom) & "," & tel);
      Ada.Text_IO.Close(fichier);

   end faire_Russie;

   procedure faire_Japon is
      type tage is range 0..120;
      tmp : Integer;
      tmp_age : tage;
      min_age : tage := 120;
      max_age : tage := 0;
      package ttabAge is new Ada.Containers.Vectors (Index_Type => Positive, Element_Type => tage);
      tabAge : ttabAge.Vector;

   begin
      Put_line ("Japon ");
      Put_line("Entrez l'age des voyageaurs. Terminez par 0.");

      -- demander l'age des voyageurs, stocker dans un vecteur
      -- quand on rentre un 0, on sort et on relit le vecteur pour
      -- indiquer a la fin le + jeune et le + vieux
--        get(tmp);
--        tmp_age := tage(tmp);
--        while tmp_age /= 0 loop
      loop
         get(tmp);
         tmp_age := tage(tmp);
         exit when tmp_age = 0;
         tabAge.append(tmp_age);
      end loop;

      for i in 1..integer(tabAge.length) loop
         if tabAge(i)>max_age then
            max_age := tabAge(i);
         end if;

         if tabAge(i)<min_age then
            min_age := tabAge(i);
         end if;

      end loop;


      Put_line("Intervalle des ages : " & min_age'Image & " - " & max_age'Image);

   end faire_Japon;

end Asie;
