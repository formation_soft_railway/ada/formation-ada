with Ada.Text_IO;
use Ada.Text_IO;

package body Europe is

   -- ##########################################################################
   --                           Faire_Espagne
   -- ##########################################################################
   procedure faire_Espagne is

      type Situation is (Terrestre, Maritime);

      -- Type enregistrement - utilise pour la programmation oriente objet
      type Emplacement is record
         longitude : float;
         latitude : float;
         endroit : Situation;
         ville : string (1..15);
      end record;

      e1_madrid : Emplacement := (-1.0, -37.2, Terrestre, "Madrid         ");

      -- ##########################################################################
      --                                    Begin
      -- ##########################################################################


   begin
      Put_line("Espagne :");
      Put_line(e1_madrid.ville & " : " & e1_madrid.longitude'Image & " " & e1_madrid.latitude'Image
               & " " & e1_madrid.endroit'Image);
   end faire_Espagne;

   -- ##########################################################################
   --                           Faire_Italie
   -- ##########################################################################
   procedure faire_Italie is

      -- ********************* Package Destination ****************************
      package Destination is

         -- destination : nom, pays, jours
         -- Tagged permet d'�tiqueter le record pour pouvoir l'utiliser pour
         -- appeler les methodes du package
         -- limited permet d'empecher la copie de l'objet
         -- type Instance is tagged limited record
         type Instance is tagged record
            nom : string (1..10);
            pays : string (1..2);
            jours : positive;
         end record;

         function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1) return Instance;
         procedure Afficher(self : in out Instance);
         procedure Allonger (self : in out Instance; ajout : integer);
         procedure Raccourcir (self : in out Instance; reduction :  positive);
         function Heures (self : Instance) return positive;
         procedure AfficherAvecEtoiles (self : in out Instance'class);

      end Destination;

      package body Destination is

         -- --------- Constructeur --------- --
         function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1)
                                return Instance is
            i : instance := (nom, pays, jours);
         begin
            return i;
         end Create;

         -- ------ Procedure Afficher ------ --
         procedure Afficher(self : in out Instance) is
         begin
            Put_line(self.nom & " " & self.pays & " " & self.jours'Image);
         end Afficher;

         -- ------ Procedure Allonger ------ --
         procedure Allonger (self : in out Instance; ajout : integer ) is
         begin
            self.jours := self.jours + ajout;
         end Allonger;

         -- ------ Procedure Raccourcir ------ --
         procedure Raccourcir (self : in out Instance; reduction : positive ) is
         begin
            self.Allonger(-reduction);
         end Raccourcir;

         -- ------ Fonction heures ------ --
         function Heures (self : Instance) return positive is
         begin
            return self.jours * 24;
         end Heures;

         -- ------ Procedure Allonger ------ --
         -- Polymorphisme
         -- la procedure afficher dependra de la classe derivee dans laquelle elle est
         -- definie grace au mot cle 'class
         procedure AfficherAvecEtoiles (self : in out Instance'class) is
         begin
            Put_Line("********************");
            self.Afficher;
            Put_Line("********************");
         end AfficherAvecEtoiles;

         -- -----------------------------------

      end Destination;

      -- ******************* Package DestinationMaritime **********************

      -- Package derive de Destination
      package DestinationMaritime is

         -- Mot cle "new" pour en faire une "classe enfant"
         -- avec un attribut supplementaire : ile
         type Instance is new Destination.Instance with record
            ile : string (1..10);
         end record;

         -- Redefinit la classe mere avec "overriding"
         overriding function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1) return Instance;
         function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1;
                         ile : string := "          ") return Instance;
         overriding procedure Afficher(self : in out Instance);

      end DestinationMaritime;

      package body DestinationMaritime is

         -- --------- Constructeur --------- --
         overriding function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1)
                                return Instance is
            i : instance := (nom, pays, jours, "          ");
         begin
            return i;
         end Create;

         -- Ecrasement du constructeur mere
         function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1;
                         ile : string := "          ")
                                return Instance is
            i : instance := (nom, pays, jours, ile);
         begin
            return i;
         end Create;

         -- ------ Procedure Afficher ------ --
         overriding procedure Afficher(self : in out Instance) is
         begin
            -- Put_line(self.nom & " " & self.pays & " " & self.jours'Image);

            -- Appel de la methode du package pere en transformant self en objet Destination
            -- Destination.Afficher(Destination.Instance(self));

            -- Transformation de self en Destination puis appel a Afficher
            Destination.Instance(self).Afficher;
            Put_line(" - " & self.ile);
         end Afficher;
         -- -----------------------------------

      end DestinationMaritime;

      -- ********************* Declaration variables ****************************

      --d1 : destination.instance := ("Venise    ", "IT", 4);
      d1 : destination.instance := destination.Create("Venise    ", "IT", 4);
      dm1 : DestinationMaritime.instance := DestinationMaritime.Create ("Palermo   ", "IT", 3, "Sicile    ");
      -- d2 est une copie de d1. Modifier d1 ne modifiera pas d2
      d2 : destination.instance := d1;

      -- ******************************** Begin *********************************
   begin

      Put_line("Italie :");
      -- Destination.Afficher(d1);
--        d1.Afficher;
--        d1.Allonger(3);
--        d1.Afficher;
--        d1.Raccourcir(2);
--        d1.Afficher;
--        Put_Line ("En heures : " & d1.heures'Image);
--        dm1.Afficher;
--        d1.AfficherAvecEtoiles;
--        dm1.AfficherAvecEtoiles;
      d1.Allonger(10);
      d2.Afficher;

   end faire_Italie;

end Europe;
