Formation ADA

Date : 01/03/2021
ADA 95 (sauf indication)
Realise avec GNAT

Git intervenant : https://gitlab.com/mbalib2/formation-ada-210301
Documentation (1995) : https://www.adaic.org/resources/add_content/standards/05rm/html/RM-TOC.html
Documentation (2012) : http://ada-auth.org/standards/12rm/html/RM-TOC.html

-- Commandes Git --
git clone https://gitlab.com/formation_soft_railway/formation-ada.git
cd formation-ada

git config --global user.name "user name"
git config --global user.email "example@email.com"

git add myFile.txt
git commit -am "New file"
git push
